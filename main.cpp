#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <sstream>
#include <iostream>

using namespace std;
using namespace sf;

//region declaration fonctions
void updateBranches(int seed);
//endregion

//region declaration variables globales
const int NBRE_BRANCHES = 6;
Sprite branches[NBRE_BRANCHES];

enum class side {GAUCHE, DROITE, NONE};
side posBranches[NBRE_BRANCHES];
//endregion


int main() {
    string dossierSource = "../";

    // Create a video mode object
    VideoMode vm(1920, 1080);

    // Create and open a window for the game
    RenderWindow window(vm, "Timber !", Style::Fullscreen);

//region variables textures
    // Create a texture to hold a graphic on the GPU
    Texture textureBackground;

    // Load a graphic into the texture
    textureBackground.loadFromFile(dossierSource + "graphics/background.png");

    // Create a sprite
    Sprite spriteBackground;

    // Attach the texture to the sprite
    spriteBackground.setTexture(textureBackground);

    // Set the spriteBackground to cover the screen
    spriteBackground.setPosition(0, 0);

    //Création de l'arbre
    Texture tex_Tree;
    tex_Tree.loadFromFile(dossierSource + "graphics/tree.png");
    Sprite spr_Tree;
    spr_Tree.setTexture(tex_Tree);
    spr_Tree.setPosition(810, 0);

    //C'est un hareng
    Texture tex_Bee;
    tex_Bee.loadFromFile(dossierSource + "graphics/bee.png");
    Sprite spr_Bee;
    spr_Bee.setTexture(tex_Bee);
    spr_Bee.setPosition(vm.width, 800);

    float beeYOrigin;
    bool beeActive = false;
    float beeSpeed = 0.0f;

    //Les nuages
    Texture tex_Cloud;
    tex_Cloud.loadFromFile(dossierSource + "graphics/cloud.png");

    int nbreNuages = 3;
    Sprite spr_Nuages[nbreNuages];
    bool nuagesActif[nbreNuages];
    float vitNuages[nbreNuages];

    for (int i = 0; i < nbreNuages; ++i)
    {
        spr_Nuages[i].setTexture(tex_Cloud);
        spr_Nuages[i].setPosition(vm.width, i * 250);
        nuagesActif[i] = false;
        vitNuages[i] = 0;
    }

    //Les branches
    Texture tex_Branch;
    tex_Branch.loadFromFile(dossierSource+"graphics/branch.png");

    for (int i = 0; i < NBRE_BRANCHES; ++i)
    {
        branches[i].setTexture(tex_Branch);
        branches[i].setPosition(-2000,-2000);
        branches[i].setOrigin(220,20);
    }

    //Et le joueur
    Texture tex_Player;
    tex_Player.loadFromFile(dossierSource+"graphics/player.png");
    Sprite spr_Player;
    spr_Player.setTexture(tex_Player);
    spr_Player.setPosition(720, 720);
    spr_Player.setScale(-1,1);

    side side_Player = side::GAUCHE;

    //Et le décès du joueur
    Texture tex_RIP;
    tex_RIP.loadFromFile(dossierSource+"graphics/rip.png");
    Sprite spr_RIP;
    spr_RIP.setTexture(tex_RIP);
    spr_RIP.setPosition(675, 2000);

    //And my axe...
    Texture tex_Axe;
    tex_Axe.loadFromFile(dossierSource+"graphics/axe.png");
    Sprite spr_Axe;
    spr_Axe.setTexture(tex_Axe);
    spr_Axe.setPosition(700,830);

    const float AXE_LEFT = 700;
    const float AXE_RIGHT = 1220;

    //La buche qui va partir
    Texture tex_Log;
    tex_Log.loadFromFile(dossierSource+"graphics/log.png");
    Sprite spr_Log;
    spr_Log.setTexture(tex_Log);
    spr_Log.setPosition(810,720);

    bool logActive = false;
    float logSpeedX = 1000;
    float logSpeedY = -1500;

//endregion

//region variables gameplay
    //Gestion du temps
    Clock clock;
    float timeSinceBegin = 0;

    float tpsOriginal = 5;

    bool enPause = true;
    bool acceptInput = false;
//endregion

//region GUI / HUD
    int score = 0;

    Text txt_Debut, txt_Score;

    Font ft_Pixel;
    ft_Pixel.loadFromFile(dossierSource + "fonts/pixel.ttf");

    txt_Debut.setFont(ft_Pixel);
    txt_Score.setFont(ft_Pixel);

    txt_Debut.setString("Press Start to begin !");
    txt_Score.setString("Score : 0");

    //Taille des lettres
    txt_Debut.setCharacterSize(75);
    txt_Score.setCharacterSize(100);

    txt_Debut.setColor(Color::White);
    txt_Score.setColor(Color::White);

    //Positionnement de l'HUD
    FloatRect textRect = txt_Debut.getGlobalBounds();

    txt_Debut.setOrigin(textRect.left + textRect.width / 2.f, textRect.top + textRect.height / 2.f);

    txt_Debut.setPosition(vm.width/2.f, vm.height/2.f);

    txt_Score.setPosition(20, 20);

    //La barre du temps
    RectangleShape barreTps;
    float barreTpsWidth = 400;
    float barreTpsHeight = 80;
    barreTps.setSize(Vector2f(barreTpsWidth, barreTpsHeight));
    barreTps.setFillColor(Color::Blue);
    barreTps.setPosition((vm.width/2.0)-(barreTpsWidth/2.0), vm.height - 100);

    float tpsRestant = tpsOriginal;
    float tpsSec = barreTpsWidth / tpsRestant;

    String phrasesMort[] = {"REKT.", "Pwnd.", "It seems you died.", "RIP in Pepperoni.", "OH NOEZ !!!1!", "In a parallel universe, you won.", "So close !",
    "Not again !", ":'("};
    int nbrePhrases = (int)phrasesMort->getSize();

//endregion

//region variables son
    SoundBuffer sndBuff_chop;
    sndBuff_chop.loadFromFile(dossierSource+"sound/chop.wav");
    Sound snd_chop;
    snd_chop.setBuffer(sndBuff_chop);

    SoundBuffer sndBuff_death;
    sndBuff_death.loadFromFile(dossierSource+"sound/death.wav");
    Sound snd_death;
    snd_death.setBuffer(sndBuff_death);

    SoundBuffer sndBuff_oot;
    sndBuff_oot.loadFromFile(dossierSource+"sound/out_of_time.wav");
    Sound snd_oot;
    snd_oot.setBuffer(sndBuff_oot);
//endregion

    while (window.isOpen())
    {

        //region player input
        /*
        ****************************************
        Handle the players input
        ****************************************
        */

        Event event;

        while(window.pollEvent(event))
        {
            if (event.type == Event::KeyReleased && !enPause)
            {
                acceptInput = true;

                spr_Axe.setPosition(2500, spr_Axe.getPosition().y);
            }
        }

        if (Keyboard::isKeyPressed(Keyboard::Escape))
            window.close();


        if (Keyboard::isKeyPressed(Keyboard::Return)) {
            enPause = false;

            //Reset du score et du temps
            score = 0;
            tpsRestant = tpsOriginal;
            timeSinceBegin = 0;

            #pragma omp parallel for
            for(int i = 1; i < NBRE_BRANCHES; ++i)
                posBranches[i] = side::NONE;

            spr_RIP.setPosition(675, 2000);
            spr_Player.setPosition(720, 720);
            spr_Player.setScale(-1,1);

            acceptInput = true;
        }

        //Gestion des inputs du joueur
        if (acceptInput)
        {
            if (Keyboard::isKeyPressed(Keyboard::Right))
            {
                side_Player = side::DROITE;
                ++score;

                tpsRestant += (2.0 / score) + .15;
                tpsRestant = tpsRestant > tpsOriginal ? tpsOriginal : tpsRestant;

                spr_Axe.setScale(-1,1);
                spr_Axe.setPosition(AXE_RIGHT, spr_Axe.getPosition().y);
                spr_Player.setPosition(1200, 720);
                spr_Player.setScale(1,1);

                updateBranches(score);

                spr_Log.setPosition(810, 720);
                logSpeedX = -5000;
                logActive = true;

                acceptInput = false;

                snd_chop.play();
            }
            else if (Keyboard::isKeyPressed(Keyboard::Left))
            {
                side_Player = side::GAUCHE;
                ++score;

                tpsRestant += (2.0 / score) + .15;
                tpsRestant = tpsRestant > tpsOriginal ? tpsOriginal : tpsRestant;

                spr_Axe.setScale(1,1);
                spr_Axe.setPosition(AXE_LEFT, spr_Axe.getPosition().y);
                spr_Player.setPosition(720, 720);
                spr_Player.setScale(-1,1);

                updateBranches(score);

                spr_Log.setPosition(810, 720);
                logSpeedX = 5000;
                logActive = true;

                acceptInput = false;

                snd_chop.play();
            }

        }

        //endregion

        //region update scene
        /*
        ****************************************
        Update the scene
        ****************************************
        */
        timeSinceBegin += .01;
        Time dt = clock.restart(); //delta time (pour savoir le temps entre deux frames)

        if (!enPause) {
            //Configuration de l'abeille
            if (!beeActive) {
                //Definition de la vitesse
                srand(time(nullptr));
                beeSpeed = (rand() % 200) + 200.0f;

                //Et de sa hauteur
                srand(time(nullptr) * 10);
                beeYOrigin = (rand() % 500) + 500.0f;
                spr_Bee.setPosition(vm.width + 100, beeYOrigin);
                beeActive = true;
            } else {
                spr_Bee.setPosition(spr_Bee.getPosition().x - (beeSpeed * dt.asSeconds()), beeYOrigin + sin(timeSinceBegin)*10);

                beeActive = spr_Bee.getPosition().x > -100.0f; //La repositionner si elle sort de l'écran
            }

            //Configuration des nuages
            #pragma omp parallel for
            for (int i = 0; i < nbreNuages; ++i) {
                if (!nuagesActif[i]) {
                    //Definition de la vitesse
                    srand(time(nullptr));
                    vitNuages[i] = (rand() % ((i + 1) * 50)) + (i + 1) * 50;

                    //Et de sa hauteur
                    spr_Nuages[i].setPosition(-400.0f, 200 - i * 75);
                    nuagesActif[i] = true;
                } else {
                    spr_Nuages[i].setPosition(spr_Nuages[i].getPosition().x + (vitNuages[i] * dt.asSeconds()),
                                              spr_Nuages[i].getPosition().y);

                    nuagesActif[i] = spr_Nuages[i].getPosition().x < vm.width + 200.0f; //La repositionner si elle sort de l'écran
                }
            }

            //MàJ du texte
            stringstream ss;
            ss << "Score : " << score;
            txt_Score.setString(ss.str());

            //Gestion barre temps
            tpsRestant -= dt.asSeconds();
            barreTps.setSize(Vector2f(tpsSec * tpsRestant, barreTpsHeight));

            //Gestion timeup
            if (tpsRestant <= 0)
            {
                enPause = true;

                txt_Debut.setString("No time left !");
                textRect = txt_Debut.getLocalBounds();

                txt_Debut.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
                txt_Debut.setPosition(window.getSize().x / 2.0f, window.getSize().y / 2.0f);

                snd_oot.play();
            }

            //MàJ branches
            #pragma omp parallel for
            for (int i = 0; i < NBRE_BRANCHES; ++i)
            {
                float height = i * 150;

                if(posBranches[i] == side::GAUCHE)
                {
                    branches[i].setPosition(610, height);
                    branches[i].setRotation(180);
                }
                else if (posBranches[i] == side::DROITE)
                {
                    branches[i].setPosition(1330, height);
                    branches[i].setRotation(0);
                }
                else
                {
                    branches[i].setPosition(3000, height);
                }
            }

            //On fait voler le bois ! (si besoin)
            if (logActive)
            {
                spr_Log.setPosition(spr_Log.getPosition().x + (logSpeedX * dt.asSeconds()), spr_Log.getPosition().y + (logSpeedY * dt.asSeconds()));

                if (spr_Log.getPosition().x < -100 || spr_Log.getPosition().x > 2000)
                {
                    logActive = false;
                    spr_Log.setPosition(810, 720);
                }
            }

            //Gestion de la mort
            if (posBranches[NBRE_BRANCHES-1] == side_Player)
            {
                enPause = true;
                acceptInput = false;

                spr_RIP.setPosition(spr_Player.getPosition().x, 760);
                spr_Player.setPosition(2500,660);

                int r = (rand() % nbrePhrases);
                txt_Debut.setString(phrasesMort[r]);

                FloatRect textRect = txt_Debut.getLocalBounds();

                txt_Debut.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
                txt_Debut.setPosition(window.getSize().x / 2.0f, window.getSize().y / 2.0f);

                snd_death.play();
            }

        } //end if(!enPause)
        //endregion

        //region render scene
        /*
        ****************************************
        Draw the scene
        ****************************************
        */

        // Clear everything from the last frame
        window.clear();

        // Draw our game scene here
        window.draw(spriteBackground);

        #pragma omp parallel for
        for (int i = 0; i < nbreNuages; ++i)
            window.draw(spr_Nuages[i]);

        #pragma omp parallel for
        for (int i = 0; i < NBRE_BRANCHES; ++i)
            window.draw(branches[i]);

        window.draw(spr_Tree);

        window.draw(spr_Player);

        window.draw(spr_Axe);

        window.draw(spr_Log);

        window.draw(spr_RIP);

        window.draw(spr_Bee);

        //Dessin du GUI
        window.draw(txt_Score);
        window.draw(barreTps);

        if (enPause)
            window.draw(txt_Debut);

        // Show everything we just drew
        window.display();

        //endregion

    }

    return 0;
}

void updateBranches(int seed)
{
    for (int j = NBRE_BRANCHES - 1; j > 0; --j)
    {
        posBranches[j] = posBranches[j-1];
    }

    //Recréation de la branche la plus haute
    srand((int)time(0) + seed);
    int r = (rand() % 5);
    switch(r)
    {
        case 0:
            posBranches[0] = side::GAUCHE;
            break;

        case 1:
            posBranches[0] = side::DROITE;
            break;

        default:
            posBranches[0] = side::NONE;
            break;
    }
}